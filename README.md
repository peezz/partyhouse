#Camook's Party House Archive/Wiki
##About

An attempt at preserving the rich culture of this historic server.

## Usage

[USERS.md](/pages/USERS.md) to view users and aliases.

[CHANNELS.md](/pages/CHANNELS.md) to view channel history and comments.

[WELCOME.md](/pages/WELCOME.md) to view welcome messages.

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) for details.

See [Template.md](Template.md) for templates and notes on how to contribute.

##TODO

* Welcome messages
* Channel comments
* Clean up channel document
* Added remainder of users