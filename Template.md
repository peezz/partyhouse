#Template
---
## For Existing Channels
---
	### Channel name
	Description
	
	**Preceded by:** _Previous channel name_
	
	**Comments:**
	
	Raj and Ray will handle images (unless you specifically want to try). 

	Add any text you can.

## For Old Channels
---

	### Channel name
	
	**Preceded by:** _Previous channel name_
	
	**Succeeded by:** _New channel name_
## Users
---
	### Username
	Other aliases:
	
	+ Name 1
	+ Name 2
	+ Name 3
	+ Name 4
	+ etc.

## Notes
---
#### New lines
New lines are done by a blank line in between content:

	Line 1

	Line 2

#### Hyperlinks
Hyperlinks are done by:

	[Text](Link)

#### Searching for channel and name changes
You can search the log files by copy-pasting the log into [here.](http://www.online-utility.org/text/grep.jsp) Then put the term you want to search for into the "Input Regular Expression" field.

If you are searching for a user, it is easiest to use their user ID. In the changelog it shows like this

	hittingray(5)

where 5 is the user ID. So searching for `user 5 to` will return:
	
	<W>2014-02-17 07:10:16.560 1 => <14:tehaidsz(20)> Renamed user 5 to 'hittinfagit'
	<W>2014-02-18 03:11:26.221 1 => <8:hittinfagit(5)> Renamed user 5 to 'hittingray'
	<W>2014-02-18 06:42:03.083 1 => <3:tehaidsz(20)> Renamed user 5 to 'hittingraymaximumfagitedition'
	<W>2014-02-18 06:43:35.843 1 => <11:hittingraymaximumfagitedition(5)> Renamed user 5 to 'hittingray'
	<W>2014-02-22 04:38:11.901 1 => <28:tehaidsz(20)> Renamed user 5 to 'hittingraymaximumfaggetedition'
	<W>2014-02-22 04:40:17.299 1 => <17:hittingraymaximumfaggetedition(5)> Renamed user 5 to 'hittingray'
	<W>2014-03-05 07:11:43.094 1 => <24:tehaidsz(20)> Renamed user 5 to 'hittingfaget'
	<W>2014-03-05 07:14:22.514 1 => <3:hittingray(5)> Renamed user 5 to 'hittingray'
	<W>2014-03-10 05:19:47.566 1 => <11:hittingray(5)> Renamed user 5 to 'Raymond'
	<W>2014-03-10 05:20:43.330 1 => <9:Raymond(5)> Renamed user 5 to 'hittingray'
	<W>2014-04-08 08:55:00.263 1 => <26:hittingray(5)> Renamed user 5 to 'Huge_E-shrek-shen'
	
Note that user IDs can change due to people forgetting certificates.
		
Simply searching for a channel name will show what it has been renamed to and from.

#### Pull requests & submission

When you are finished editing, click commit in the bottom right. If you edit a file, unless you have permission to commit, it will ask you to put in a pull request instead. Click yes. If you don't know how to do this, just sent Raj or Ray a .txt or Pastebin with what you've done.