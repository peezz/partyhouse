#!/usr/bin/env bash
# script to extract user and channel information from mumble log
#
# usage: filter.sh <mumble log>
# eg:
# filter.sh mumble-216.127.64.165-6650.log

cat $1 | grep "Renamed user" > user_changelog
cat $1 | grep "Renamed channel \| Removed channel \| Added channel" > channel_changelog
