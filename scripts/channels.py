# translates changelog to wiki markdown
# does not handle rename cycles (only remembers first instance of channel)
# does not handle hierarchy
# lets do this shit

# Helpers

def add_name(line, seen):
    name = ''.join(line.split("Added channel ")[1:]).split("[")[0] # wat
    if name not in seen: # no dupes
        ch = Channel(name)
        return ch
    else:
        return None

def change_name(line, seen):
    name = ''.join(line.split("Renamed channel ")[1:]).split("] to ")[1].strip('\n') # double the madness, double the fun
    before = ''.join(line.split("Renamed channel ")[1:]).split("[")[0]
    if name not in seen: # no dupes
        ch = Channel(name, before)
        return ch
    else:
        return None

def channel_action(s):
    if "Added channel" in s:
        return "A"
    if "Renamed channel" in s:
        return "R"

def parse_dat_shit(log, target):
    dummy = Channel("dummy")
    channels = {}
    with log as f:
        for line in f:
            if channel_action(line) == "A":
                ch = add_name(line, channels)
                if ch != None:
                    channels[ch.name] = ch
            if channel_action(line) == "R":
                ch = change_name(line, channels)
                if ch != None:
                    channels[ch.name] = ch
                    channels.get(ch.before, dummy).after = ch.name
    return channels

def print_that_shit(channels, target):
    with target as f:
        for name, chan in channels.items():
            print("{} \n {} : {}".format(chan.name, chan.before, chan.after))
            target.write("###{}\n\n".format(chan.name))
            target.write("Preceeded by: _{}_\n\n".format(chan.before))
            if chan.after != None:
                target.write("Succeded By: _{}_\n\n".format(chan.after))
            target.write("\n")
# data
class Channel(object):
    def __init__(self, name, before = None):
        self.name = name
        self.before = before
        self.after = None
#########################

log = open("channel_changelog")
target = open("channels_out.md", "w+")

channels = parse_dat_shit(log, target)
print_that_shit(channels, target)
