# Dumps name changes for each userid to markdown
# quick 'n dirty

def gen_user(num, target):
    log = "user_changelog"
    target.write("### {}\n\n".format(num))
    with open(log) as f:
        thing = " {} ".format(num)
        for line in f:
            if thing in line.split("=")[1]:
                target.write("+ {}\n".format(line.split("\'")[1]))

def doit():
    for i in range(60):
        fname = "{}.md".format(i)
        target = open(fname, "w+")
        gen_user(i, target)
doit()
